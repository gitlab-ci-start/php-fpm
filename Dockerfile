FROM php:7.3-fpm-alpine

# Set Timezone
ENV TIMEZONE Asia/Taipei
RUN apk update && \
    apk add --no-cache ca-certificates \
        tzdata && \
    ln -snf /usr/share/zoneinfo/$TIMEZONE /etc/localtime && \
    echo $TIMEZONE > /etc/timezone

# Install Dependencies
RUN apk add --no-cache \
        g++ \
        make \
        autoconf \
        freetype-dev \
        libmcrypt-dev \
        libpng-dev \
        libjpeg-turbo-dev \
        libxml2-dev \
        libxslt-dev \
        libzip-dev \
        yaml-dev \
        zlib-dev

# Install Core Extensions
RUN docker-php-ext-configure gd \
        --with-gd \
        --with-freetype-dir=/usr/include/ \
        --with-png-dir=/usr/include/ \
        --with-jpeg-dir=/usr/include/ && \
    docker-php-ext-configure xml \
        --with-libxml-dir=/usr/include/ && \
    docker-php-ext-configure xsl \
        --with-libxslt-dir=/usr/include/ && \
    docker-php-ext-configure zip \
        --with-zlib-dir=/usr && \
    NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) && \
    docker-php-ext-install -j${NPROC} \
        bcmath \
        gd \
        xml \
        xsl \
        zip \
        pdo \
        pdo_mysql \
        mysqli

# Remove Dependencies
RUN apk del g++ make autoconf

# Show PHP Info
RUN php -i
